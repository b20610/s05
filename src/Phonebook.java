import java.util.ArrayList;

public class Phonebook {

    private ArrayList<Contact> contacts = new ArrayList<>();

    public Phonebook() {
    }

    public Phonebook(ArrayList<Contact> contacts) {
        this.contacts = contacts;
    }

    public ArrayList<Contact> getContacts() {
        return contacts;
    }

    public void setContacts(Contact contact) {

        this.contacts.add(contact);
    }

    public void displayContacts() {

//        System.out.println(this.contacts);
        if(this.contacts.size() == 0){
            System.out.println("The phonebook is currently empty.");
        } else {
            System.out.println("-----------------");
            for (Contact contact : contacts) {
                System.out.println(contact.getName() + " has the following registered number:");
                System.out.println(contact.getContactNumber());
                System.out.println(contact.getName() + " has the following registered address:");
                System.out.println(contact.getAddress());
                System.out.println("-----------------");
            }
        }
    }
}
