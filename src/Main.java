import java.util.Arrays;

public class Main {
    public static void main(String[] args) {

//        System.out.println("Hello world!");

        Phonebook phonebook = new Phonebook();
        Contact contact1 = new Contact("Lisa Manoban","09999962690","Mandaluyong");
        Contact contact2 = new Contact("Josef Manoban","09277729290","Mandaluyong");

        phonebook.setContacts(contact1);
        phonebook.setContacts(contact2);

//        System.out.println(phonebook.getContacts());
        phonebook.displayContacts();


    }
}